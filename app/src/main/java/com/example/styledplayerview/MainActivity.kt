package com.example.styledplayerview

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.ui.StyledPlayerView

class MainActivity : AppCompatActivity() {

    private lateinit var styledPlayerView:StyledPlayerView
    private lateinit var exoPlayer: ExoPlayer
    private lateinit var fullScreen:ImageView
    private var isFullScreen:Boolean = false;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        styledPlayerView = findViewById(R.id.playerView)
        fullScreen = findViewById(R.id.full_screen)
        exoPlayer = ExoPlayer.Builder(this).setSeekBackIncrementMs(10000).setSeekForwardIncrementMs(10000).build();
        val mediaItem: MediaItem = MediaItem.Builder()
            .setUri("https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4")
            .setMediaId("test")
            .build()

        exoPlayer.addMediaItem(mediaItem)
        styledPlayerView.player = exoPlayer
        exoPlayer.prepare()
        exoPlayer.playWhenReady = true

        fullScreen.setOnClickListener {
            if (isFullScreen) {
                fullScreen.setImageResource(R.drawable.ic_fullscreen)
                window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
                requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
                val params = styledPlayerView.layoutParams as ConstraintLayout.LayoutParams
                params.width = ViewGroup.LayoutParams.MATCH_PARENT
                params.height = (200 * applicationContext.resources.displayMetrics.density).toInt()
                styledPlayerView.layoutParams = params


                isFullScreen = false
            }
            else {
                fullScreen.setImageDrawable(
                    ContextCompat.getDrawable(
                        this@MainActivity,
                        R.drawable.ic_fullscreen_exit
                    )
                )
                window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_FULLSCREEN
                        or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION)

                requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                val params = styledPlayerView.layoutParams as ConstraintLayout.LayoutParams
                params.width = ViewGroup.LayoutParams.MATCH_PARENT
                params.height = ViewGroup.LayoutParams.MATCH_PARENT
                styledPlayerView.layoutParams = params


                isFullScreen = true
            }
        }

    }
}